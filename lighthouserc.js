module.exports = {
    ci: {
        collect: {
            numberOfRuns: 3,
            startServerCommand: "npm start",
            url: [
                "https://www.gupshup.io/partners",
                // "https://www.gupshup.io"
            ],
            settings: {
                onlyCategories: [
                    "performance",
                    "accessibility",
                    "best-practices",
                    "seo"
                ],
                emulatedFormFactor: 'desktop',
                output: ['html'],
                skipAudits: ['uses-http2'],
                chromeFlags: '--no-sandbox',
                extraHeaders: JSON.stringify({
                    Cookie: 'customCookie'
                })
            }
        },
        assert: {
            assertions: {
                'categories: performance': [
                    'error',
                    {
                        minScore: 0.9, aggregationMethos: 'medium-run'
                    }
                ],
                'categories: accessibility': [
                    'error',
                    {
                        minScore: 0.9, aggregationMethos: 'pessimistic'
                    }
                ],
                'categories: best-practices': [
                    'error',
                    {
                        minScore: 0.9, aggregationMethos: 'pessimistic'
                    }
                ],
                'categories: seo': [
                    'error',
                    {
                        minScore: 0.9, aggregationMethos: 'pessimistic'
                    }
                ]
            }
        },
        upload: {
            target: 'temporary-public-storage'
        }
    }
}